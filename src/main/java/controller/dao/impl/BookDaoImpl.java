package controller.dao.impl;

import controller.dao.LibraryDao;
import library.Book;
import util.HibernateUtil;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;

/**
 *  BookDaoImpl is the class which provides access to database records.
 *  @see LibraryDao
 *  @see HibernateUtil
 */
public class BookDaoImpl implements LibraryDao<Book> {

    /**
     * The method used for adding some record in database table.
     *
     * @param book the instance of Book class which will be added as record in database
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public void add(Book book) throws SQLException {

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.save(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for deleting some record from database.
     *
     * @param book the instance of Author class which will be deleted from database.
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public void delete(Book book) throws SQLException {

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.delete(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for getting some record from database.
     *
     * @param key the primary key of object which will be gotten from database.
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public Book get(int key) throws SQLException {

        /**
         * A variable representing instance of Book class.
         */
        Book book = null;

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            book = session.get(Book.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return book;
    }

    /**
     * The method used for getting all records from database. Has no input parameters.
     *
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public List<Book> getAll() throws SQLException {

        /**
         * A variable representing list of Book objects.
         */
        List<Book> books = null;

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            books = session.createCriteria(Book.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return books;
    }
}
