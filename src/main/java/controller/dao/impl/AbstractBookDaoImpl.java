package controller.dao.impl;

import controller.dao.LibraryDao;
import library.AbstractBook;
import org.hibernate.Session;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.List;

/**
 *  AbstractBookDaoImpl is the class which provides access to database records.
 *  @see LibraryDao
 *  @see HibernateUtil
 */
public class AbstractBookDaoImpl implements LibraryDao<AbstractBook> {

    /**
     * The method used for adding some record in database table.
     * @param abstractBook the instance of AbstractBook class which will be added as record in database
     * @exception SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public void add(AbstractBook abstractBook) throws SQLException {

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.save(abstractBook);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for deleting some record from database table.
     * @param abstractBook the instance of AbstractBook class which will be deleted from database
     * @exception SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other errors.
     */
    public void delete(AbstractBook abstractBook) throws SQLException {

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.delete(abstractBook);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for getting some record from database table.
     * @param key the primary key of object which will be gotten from database
     * @exception SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other errors.
     */
    public AbstractBook get(int key) throws SQLException {

        /**
         * A variable representing instance of AbstractBook class.
         */
        AbstractBook abstractBook = null;

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            abstractBook = session.get(AbstractBook.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return abstractBook;
    }

    /**
     * The method used for getting all objects from database. Has no input parameters.
     * @exception SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other errors.
     */
    public List<AbstractBook> getAll() throws SQLException {

        /**
         * A variable representing list of AbstractBook objects.
         */
        List<AbstractBook> abstractBooks = null;

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            abstractBooks = session.createCriteria(AbstractBook.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return abstractBooks;
    }
}
