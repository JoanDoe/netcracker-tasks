package controller.dao.impl;

import controller.dao.LibraryDao;
import library.Author;
import org.hibernate.Session;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * AuthorDaoImpl is the class which provides access to database records.
 *
 * @see LibraryDao
 * @see HibernateUtil
 */
public class AuthorDaoImpl implements LibraryDao<Author> {

    /**
     * The method used for adding some record in database table.
     *
     * @param author the instance of Author class which will be added as record in database
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public void add(Author author) throws SQLException {

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.save(author);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for deleting some record from database.
     *
     * @param author the instance of Author class which will be deleted from database
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public void delete(Author author) throws SQLException {

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            session.delete(author);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    /**
     * The method used for getting some record from database.
     *
     * @param key the primary key of object which will be gotten from database.
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public Author get(int key) throws SQLException {

        /**
         * A variable representing instance of Author class.
         */
        Author author = null;

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            author = session.get(Author.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return author;
    }

    /**
     * The method used for getting all records from database. Has no input parameters.
     *
     * @throws SQLException an exception thrown by this method. The reason of exception generation are problems with database access or other error.
     */
    public List<Author> getAll() throws SQLException {

        /**
         * A variable representing list of Author objects.
         */
        List<Author> authors = null;

        /**
         * A variable representing instance of Session class.
         */
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            authors = session.createCriteria(Author.class).list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
        return authors;
    }
}
