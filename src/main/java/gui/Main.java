package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * class description comment
 */
public class Main extends Application {
    /* class implementation comment*/

    /** variable doc comment*/
    private final static String STAGE_TITLE = "Library";

    /** variable doc comment*/
    private final static String FXML_PATH = "gui.fxml";

    /**method doc comment*/
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    /**method doc comment*/
    public void start(Stage primaryStage) throws Exception {

        /** variable doc comment*/
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(FXML_PATH));

        primaryStage.setTitle(STAGE_TITLE);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
