package gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * class description comment
 */
public class LibraryController implements Initializable {

    @FXML
    /** variable doc comment*/
    private ChoiceBox DBChoiceBox;

    @FXML
    /** variable doc comment*/
    private TableView dataTableView;

    @FXML
    /** variable doc comment*/
    private Button createButton;

    @FXML
    /** variable doc comment*/
    private Button updateButton;

    @FXML
    /** variable doc comment*/
    private Button deleteButton;

    // @Override
    /**method doc comment*/
    public void initialize(URL url, ResourceBundle resourceBundle) {}
}
