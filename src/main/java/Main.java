import controller.dao.impl.AbstractBookDaoImpl;
import controller.dao.impl.AuthorDaoImpl;
import controller.dao.impl.BookDaoImpl;
import general.Factory;
import library.AbstractBook;
import library.Author;
import library.Book;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by �� on 11/1/2015.
 */
public class Main {

	public static  void main(String[] args) throws SQLException {
		Factory factory=Factory.getInstance();
		AuthorDaoImpl authorDao=factory.getAuthorDao();
		AbstractBookDaoImpl abstractBookDao=factory.getAbstractBookDao();
		BookDaoImpl bookDao=factory.getBookDao();

//		Author author=new Author();
//		author.setId(1);
//		author.setFirstName("Lev");
//		author.setLastName("Tolstoy");
//		authorDao.add(author);
//
//		author.setId(2);
//		author.setFirstName("Alexander");
//		author.setLastName("Pushkin");
//		authorDao.add(author);
//
//		author.setId(3);
//		author.setFirstName("Mihail");
//		author.setLastName("Bulgakov");
//		authorDao.add(author);

//		AbstractBook abstractBook=new AbstractBook();
//		abstractBook.setTitle("ttitle 1");
//		abstractBook.setAuthor((Author) authorDao.get(161));
//		abstractBookDao.add(abstractBook);
//
//		abstractBook.setTitle("ttitle 2");
//		abstractBook.setAuthor((Author)authorDao.get(162));
//		abstractBookDao.add(abstractBook);
//
//		abstractBook.setTitle("ttitle 3");
//		abstractBook.setAuthor((Author)authorDao.get(163));
//		abstractBookDao.add(abstractBook);
//
//
//		abstractBook.setTitle("ttitle 4");
//		abstractBook.setAuthor((Author)authorDao.get(162));
//		abstractBookDao.add(abstractBook);
//
//		Book book=new Book();
//		book.setBook((AbstractBook)abstractBookDao.get(193));
//		book.setIsbn(33424);
//		book.setPagesQuantity(112);
//		bookDao.add(book);
//
//		book.setBook((AbstractBook) abstractBookDao.get(194));
//		book.setIsbn(33255);
//		book.setPagesQuantity(352);
//		bookDao.add(book);
//
//		book.setBook((AbstractBook) abstractBookDao.get(195));
//		book.setIsbn(36244);
//		book.setPagesQuantity(342);
//		bookDao.add(book);
//
//		book.setBook((AbstractBook) abstractBookDao.get(225));
//		book.setIsbn(36234);
//		book.setPagesQuantity(442);
//		bookDao.add(book);

		List<Author> authors=authorDao.getAll();
		System.out.println("AUTHORs");
		System.out.println("id		firstName		LastName");
		for (Author authr : authors){

			System.out.println(authr.getId()+" 		 "+ authr.getFirstName()+" 		 "+ authr.getLastName());
		}

		List<AbstractBook> abstractBooks=abstractBookDao.getAll();
		System.out.println("AbctractBooks");
		for (AbstractBook abstrctBook : abstractBooks){
			Author athr=abstrctBook.getAuthor();
			System.out.println(abstrctBook.getId() + "  " + abstrctBook.getTitle() + " 		 " + athr.getId() + "		" + athr.getFirstName() + "		" + athr.getLastName());
		}

		List<Book> books=bookDao.getAll();
		System.out.println("Books");
		System.out.println("book_id		ISBN	pages	abstract_book_id	title		author_id	firstName	LastName");
		for (Book bk : books){
			AbstractBook abstrctBook=bk.getBook();
			Author athr=abstrctBook.getAuthor();
			System.out.println(bk.getId()+"			"+bk.getIsbn()+"	 "+bk.getPagesQuantity()+"			"+ abstrctBook.getId() +
					" 		" + abstrctBook.getTitle() + "	  " + athr.getId() + "		" + athr.getFirstName() + "	" + athr.getLastName());
		}
	}
}
