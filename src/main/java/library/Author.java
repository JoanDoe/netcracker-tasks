package library;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "authors")
/**
 * class description comment
 */
public class Author {
    /* class implementation comment*/

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    /** variable doc comment*/
    private int id;

    @Column(name = "first_name")
    /** variable doc comment*/
    private String firstName;

    @Column(name = "last_name")
    /** variable doc comment*/
    private String lastName;

    @Column(name = "birth_date")
    /** variable doc comment*/
    private Date birthDate;

    @Column(name = "death_date")
    /** variable doc comment*/
    private Date deathDate;

    @Column(name = "nationality")
    /** variable doc comment*/
    private String nationality;

    /**method doc comment*/
    public int getId() {
        return id;
    }

    /**method doc comment*/
    public void setId(int id) {
        this.id = id;
    }

    /**method doc comment*/
    public String getFirstName() {
        return firstName;
    }

    /**method doc comment*/
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**method doc comment*/
    public String getLastName() {
        return lastName;
    }

    /**method doc comment*/
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**method doc comment*/
    public Date getBirthDate() {
        return birthDate;
    }

    /**method doc comment*/
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**method doc comment*/
    public Date getDeathDate() {
        return deathDate;
    }

    /**method doc comment*/
    public void setDeathDate(Date deathDate) {
        this.deathDate = deathDate;
    }

    /**method doc comment*/
    public String getNationality() {
        return nationality;
    }

    /**method doc comment*/
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
