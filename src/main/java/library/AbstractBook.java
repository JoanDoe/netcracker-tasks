package library;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "abstract_books")
/**
 *  Abstract book is the class representing.
 *  @see LibraryDao
 *  @see HibernateUtil
 */
public class AbstractBook {
    /* class implementation comment*/

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    /** variable doc comment*/
    private int id;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    /** variable doc comment*/
    private Author author;

    @Column(name = "title")
    /** variable doc comment*/
    private String title;

    @Column(name = "publication_date")
    /** variable doc comment*/
    private Date publicationDate;

    @Column (name = "genre")
    /**variable doc comment*/
    private String genre;

    /**method doc comment*/
    public Author getAuthor() {
        return author;
    }

    /**method doc comment*/
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**method doc comment*/
    public String getTitle() {
        return title;
    }

    /**method doc comment*/
    public void setTitle(String title) {
        this.title = title;
    }

    /**method doc comment*/
    public Date getPublicationDate() {
        return publicationDate;
    }

    /**method doc comment*/
    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    /**method doc comment*/
    public int getId() {
        return id;
    }

    /**method doc comment*/
    public void setId(int id) {
        this.id = id;
    }

    /**method doc comment*/
    public String getGenre() {
        return genre;
    }

    /**method doc comment*/
    public void setGenre(String genre) {
        this.genre = genre;
    }
}