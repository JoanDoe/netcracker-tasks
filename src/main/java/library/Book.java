package library;

import javax.persistence.*;

@Entity
@Table(name = "Books")
/**
 * class description comment
 */
public class Book {
    /* class implementation comment*/

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    /** variable doc comment*/
    private int id;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "abstract_book_id")
    /** variable doc comment*/
    private AbstractBook book;

    @Column(name = "duplicate_count")
    /** variable doc comment*/
    private int duplicateCount;

    @Column(name = "pages_quantity")
    /** variable doc comment*/
    private int pagesQuantity;

    @Column(name = "is_given")
    /** variable doc comment*/
    private boolean isGiven;

    @Column(name="publishing_house")
    /** variable doc comment*/
    private String publishingHouse;

    @Column (name="language")
    /** variable doc comment*/
    private String language;

    @Column (name ="isbn")
    /**variable doc comment*/
    private int isbn;

    /**method doc comment*/
    public int getId() {
        return id;
    }

    /**method doc comment*/
    public void setId(int id) {
        this.id = id;
    }

    /**method doc comment*/
    public AbstractBook getBook() {
        return book;
    }

    /**method doc comment*/
    public void setBook(AbstractBook book) {
        this.book = book;
    }

    /**method doc comment*/
    public int getDuplicateCount() {
        return duplicateCount;
    }

    /**method doc comment*/
    public void setDuplicateCount(int duplicateCount) {
        this.duplicateCount = duplicateCount;
    }

    /**method doc comment*/
    public boolean isGiven() {
        return isGiven;
    }

    /**method doc comment*/
    public void setIsGiven(boolean isGiven) {
        this.isGiven = isGiven;
    }

    /**method doc comment*/
    public String getPublishingHouse() {
        return publishingHouse;
    }

    /**method doc comment*/
    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    /**method doc comment*/
    public int getPagesQuantity() {
        return pagesQuantity;
    }

    /**method doc comment*/
    public void setPagesQuantity(int pagesQuantity) {
        this.pagesQuantity = pagesQuantity;
    }

    /**method doc comment*/
    public String getLanguage() {
        return language;
    }

    /**method doc comment*/
    public void setLanguage(String language) {
        this.language = language;
    }

    /**method doc comment*/
    public int getIsbn() {
        return isbn;
    }

    /**method doc comment*/
    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }
}
